import { useState } from 'react';
import './App.css'
import { Card } from './Components/card/card';
import { useFoodData } from './hooks/useFoodData';
import { CreateModal } from './Components/card/create-modal/create-modal';

function App() {
  const {data} = useFoodData();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const handleOpenModal = () =>{
    setIsModalOpen(prev => !prev)
  }

  return (
<div>
    <div className='headers'>
      
      <h2 className='logo'><img src="src\staticsgeral\Logo.jpg" alt="" /></h2>
      <h1 className='empresa'>Delicias da Di</h1>
      <nav className='navigation'>
        <a href="#">Home</a>
        {isModalOpen && <CreateModal/>}
        <a onClick={handleOpenModal}>Novos produtos</a>
        <a href="#">Contatos</a>
        <button className="buttonlogin">Login</button>
      </nav>
      
    </div>
      <div className="container">
        
       <div className="card-grid">
        {data?.map(foodData => 
          <Card 
          price={foodData.price} 
          title={foodData.title} 
          image={foodData.image}
          />)}
          
       </div>
       
      </div>
      </div>
  )
}

export default App
