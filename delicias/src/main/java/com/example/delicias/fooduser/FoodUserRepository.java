package com.example.delicias.fooduser;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodUserRepository extends JpaRepository <FoodUser,Long> {

}
