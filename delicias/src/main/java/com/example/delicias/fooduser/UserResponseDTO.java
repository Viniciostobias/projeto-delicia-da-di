package com.example.delicias.fooduser;

public record UserResponseDTO(Long id,String nome,String cpf,String email, int senha) {
    public UserResponseDTO(FoodUser foodUser){
        this(foodUser.getId(), foodUser.getNome(),foodUser.getCpf(), foodUser.getEmail(), foodUser.getSenha());
    }
}
