package com.example.delicias.fooduser;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Table(name="fooduser")
@Entity(name="fooduser")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")

public class FoodUser {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String nome;

    private String cpf;

    private String email;

    private int senha;


    public FoodUser(UserRequestDTO userConstrutor) {
          this.id = userConstrutor.id();
          this.nome = userConstrutor.nome();
          this.cpf = userConstrutor.cpf();
          this.email = userConstrutor.email();
          this.senha = userConstrutor.senha();
    }
}
