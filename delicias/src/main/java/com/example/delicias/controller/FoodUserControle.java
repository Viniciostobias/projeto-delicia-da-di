package com.example.delicias.controller;
import com.example.delicias.food.FoodResponseDTO;
import com.example.delicias.fooduser.FoodUser;
import com.example.delicias.fooduser.FoodUserRepository;
import com.example.delicias.fooduser.UserRequestDTO;
import com.example.delicias.fooduser.UserResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("fooduser")
public class FoodUserControle {
    @Autowired
    private FoodUserRepository repository;
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping
    public void SaveUser(@RequestBody UserRequestDTO UserConstrutor) {
        FoodUser User = new FoodUser(UserConstrutor);
        repository.save(User);
        return;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping
    public List<UserResponseDTO> getAll(){
        List<UserResponseDTO> foodUserList = repository.findAll().stream().map(UserResponseDTO::new).toList();
        return foodUserList;

    }




}
