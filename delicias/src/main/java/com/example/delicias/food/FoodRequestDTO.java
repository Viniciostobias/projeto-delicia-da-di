package com.example.delicias.food;

public record FoodRequestDTO(String title, String image, Integer price) {
}
